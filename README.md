## Description

A small RESTful API to support new user tracking software.
Data does not need to be persisted between server restarts.

Powered by [Nest](https://github.com/nestjs/nest) framework.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# incremental rebuild (webpack)
$ npm run webpack
$ npm run start:hmr

# production mode
npm run start:prod

#OpenAPI (Swagger)
- Navigate to http://localhost:3000/api
```
## License

  Nest is [MIT licensed](LICENSE).
