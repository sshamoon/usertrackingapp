import { Module } from '@nestjs/common';

import { UserController } from 'User/user.controller';
import { UserService } from 'User/user.service';
import { IsUserAlreadyExist } from 'User/IsUserAlreadyExist';
import { IsPhoneNumberValid } from 'User/isPhoneNumberValid';

@Module({
  imports: [],
  controllers: [UserController],
  providers: [UserService, IsUserAlreadyExist, IsPhoneNumberValid],
})
export class AppModule {}
