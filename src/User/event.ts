import { Moment } from "moment";

export class Event {
    type: string;
    created: Moment;
}