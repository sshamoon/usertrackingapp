import { IsUserAlreadyExist } from './IsUserAlreadyExist';
import { IsNotEmpty, MaxLength, MinLength, 
    Validate, IsEmail, IsOptional, IsString } from 'class-validator';

import { IsPhoneNumberValid } from './isPhoneNumberValid';

export class UserDto {
    @IsNotEmpty()
    @MaxLength(100)
    @Validate(IsUserAlreadyExist)
    @IsEmail()
    @IsString()
    readonly email: string;
    
    @IsNotEmpty()
    @MaxLength(100)
    @MinLength(8)
    @IsString()
    readonly password: string;

    @IsString()
    @IsOptional()
    @Validate(IsPhoneNumberValid)
    readonly phoneNumber: string;
}