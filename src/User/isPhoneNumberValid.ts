import {Injectable} from '@nestjs/common';
import {ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from 'class-validator';

@ValidatorConstraint({ name: 'IsPhoneNumberValid', async: true })
@Injectable()
export class IsPhoneNumberValid implements ValidatorConstraintInterface {

	async validate(phoneNumber: string) {
        if(phoneNumber == null || 
            phoneNumber == undefined || 
            phoneNumber.trim().length == 0)
            return true;
            
        return new RegExp("^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$").test(phoneNumber.trim());
    }
    
    defaultMessage(args: ValidationArguments) {
        return "Phone number must match xxx-xxx-xxxx format.";
    }
}