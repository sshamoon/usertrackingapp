import {Injectable, Inject} from '@nestjs/common';
import {ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from 'class-validator';

import {UserService} from './user.service';

@ValidatorConstraint({ name: 'isUserAlreadyExist', async: true })
@Injectable()
export class IsUserAlreadyExist implements ValidatorConstraintInterface {
	constructor(@Inject('UserService') private readonly userService: UserService) {}

	async validate(email: string) {
        const user = await this.userService.findUser(email);
		return user == null;
    }
    
    defaultMessage(args: ValidationArguments) {
        return 'User with this email already exists.';
    }
}