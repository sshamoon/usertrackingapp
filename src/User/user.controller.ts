import { Controller, Get, Post, Param, HttpCode, Body, ValidationPipe} from '@nestjs/common';

import { UserService } from './user.service';
import { UserDto } from './userDto';
import { Event } from './event';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post("register")
  @HttpCode(200)
  registerUser(@Body(new ValidationPipe()) user: UserDto): string {
    return this.userService.createUser(user);
  }

  @Post("login")
  @HttpCode(200)
  login(@Body() cred: any): string {
    return this.userService.login(cred.email, cred.password);
  }

  @Get("getAllevents")
  getAllEvents(): Array<Event> {
    return this.userService.getAllEvents();
  }

  @Get("getLastDayEvents")
  getLastDayEvents(): Array<Event> {
    return this.userService.getLastDayEvents();
  }

  @Get("getEventsByUserEmail:email")
  getEventsByUserEmail(@Param("email") email: string): Array<Event> {
    return this.userService.getEventsByUserEmail(email);
  }
}