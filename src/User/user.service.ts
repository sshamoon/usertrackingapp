import { Injectable } from '@nestjs/common';

import * as moment from 'moment';
import * as _ from 'underscore';

import { UserDto } from './userDto';
import { User } from './user';
import { Event } from './event';


@Injectable()
export class UserService {
  login(email: string, password: string): string {
    try {
      const user = _.find(this.users, u => u.email == email)
      if(user == null || user == undefined)
        throw new Error("User not found"); 
  
      const isMatch = user.password == password;
      if(isMatch){
        user.events.push({
            type: "LOGIN",
            created: moment()
          } as Event
        )
        return "Success login";
      }
  
      return "Invalid login";
    } catch (err) {
      return err.message
    }
  }

  createUser(user: UserDto): string {
    try{
      this.users.push({
        email: user.email,
        password: user.password,
        phoneNumber: user.phoneNumber,
        events: [
          {
            type: "CREATE",
            created: moment() 
          } as Event
        ] as Array<Event>
      } as User)
  
      return "User has been created.";
    }catch (err){
      return err.message
    }
  }

  getAllEvents(): Array<Event> {
    const events = _.chain(this.users)
                    .map(u => u.events)
                    .flatten()
                    .value();
    return events;
  }

  getLastDayEvents(): Array<Event> {
    const events = _.chain(this.users)
                    .map(u => u.events)
                    .flatten()
                    .value();

    const maxDate = moment.max(_.map(events, e => e.created)).format("L")
    const lastDates = _.filter(events, (e: Event) => {
        const dateOne = e.created.format("L")
        return moment(dateOne, "MM-DD-YYYY")
              .isSame(moment(maxDate, "MM-DD-YYYY"))
    });

    return lastDates;
  }

  getEventsByUserEmail(email: string): Array<Event> {
    if(!_.any(this.users))
      return [];

    const user = this.findUser(email);
    if(user == null || user == undefined)
      return [];

    return user.events;
  }

  findUser(email: string): User {
    return _.find(this.users, u => u.email == email)
  }

  private users: Array<User> = [] as Array<User>;
}