import { Event } from './event';

export class User {
    email: string;
    password: string;
    phoneNumber: string;
    events: Array<Event>;
}